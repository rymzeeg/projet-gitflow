package sujet;

public class Sujet {
	
private String nom;
private Enseignant proposeur;

public Sujet(String nom,Enseignant e) {
	this.nom=nom;
	this.proposeur=e;
}
public Enseignant getProposeur() {
	return this.proposeur;
}

public void setProposeur(Enseignant proposeur) {
	this.proposeur = proposeur;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}

}
