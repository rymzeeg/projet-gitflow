package sujet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unused")
public class Main{
	public static void main(String[] args) {
	    System.out.println("Hello world!");
	    //Enseignant en2=new Enseignant(1,"Volt","Gabriel");
	    //Sujet IA=new Sujet("email",en1);
	    //Groupe GrA=new Groupe(1,IA,en1,en2,new ArrayList<Voeux>());
	    //Etudiant etu2 =new Etudiant(2,"DeBeauvoir","Simone",GrA);
	   
	    
	    ObjectMapper objectMapper = new ObjectMapper();
	    Enseignant en1=new Enseignant(1,"Grenet","Bruno");
	    Enseignant en2=new Enseignant(2,"Volt","Gabriel");
	    File fichier=new File("target/f.json");
	    
	    
		try {
			objectMapper.writeValue(fichier, en1);
			objectMapper.writeValue(fichier,en2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Enseignant e=objectMapper.readValue(fichier,Enseignant.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    }
}
